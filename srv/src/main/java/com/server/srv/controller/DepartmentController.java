package com.server.srv.controller;

import com.server.srv.model.Department;
import com.server.srv.service.DepartmentService;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {

	private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    // LIST
    @GetMapping
    public ResponseEntity<List<Department>> findAll() {
        return ResponseEntity.ok(departmentService.findAll());
    }

    // CREATE
    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody Department department) {
        return ResponseEntity.ok(departmentService.save(department));
    }

    // READ
    @GetMapping("/{id}")
    public ResponseEntity<Department> findById(@PathVariable Long id) {
        Optional<Department> dept = departmentService.findById(id);
        if (!dept.isPresent()) {
            log.error("Id " + id + " does not exist");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(dept.get());
    }

    // UPDATE
    @PutMapping("/{id}")
    public ResponseEntity<Department> update(@PathVariable Long id, @Valid @RequestBody Department department) {
        if (!departmentService.findById(id).isPresent()) {
            log.error("Id " + id + " does not exist");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(departmentService.save(department));
    }

    // DELETE
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        if (!departmentService.findById(id).isPresent()) {
            log.error("Id " + id + " does not exist");
            ResponseEntity.badRequest().build();
        }

        departmentService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
