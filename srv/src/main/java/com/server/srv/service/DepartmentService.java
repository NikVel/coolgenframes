package com.server.srv.service;

import com.server.srv.model.Department;
import com.server.srv.repository.DepartmentRepository;

import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRespository) {
        this.departmentRepository = departmentRespository;
    }

    // LIST
    public List<Department> findAll() {
        return departmentRepository.findByOrderByShortNameAsc();
    }

    // CREATE/UPDATE
    public Department save(Department dep) {
        return departmentRepository.save(dep);
    }

    // READ
    public Optional<Department> findById(Long id) {
        return departmentRepository.findById(id);
    }

    // DELETE
    public void deleteById(Long id) {
        departmentRepository.deleteById(id);
    }

}
